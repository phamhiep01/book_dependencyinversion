using Microsoft.EntityFrameworkCore;
using WebBook;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.


builder.Services.AddDbContext<LibraryDbContext>(options =>
               options.UseSqlServer("Data Source=DESKTOP-9FP7ODG;Database=MovieApp;Trusted_Connection=True;TrustServerCertificate=True"));
builder.Services.AddScoped<IUnitOfWork, UnitOfWork>();
builder.Services.AddControllers();
var app = builder.Build();


// Configure the HTTP request pipeline.

app.UseAuthorization();

app.MapControllers();

app.Run();
