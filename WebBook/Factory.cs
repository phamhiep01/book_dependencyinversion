﻿using Microsoft.EntityFrameworkCore.Design;
using Microsoft.EntityFrameworkCore;

namespace WebBook
{
    public class BloggingContextFactory : IDesignTimeDbContextFactory<LibraryDbContext>
    {
        public LibraryDbContext CreateDbContext(string[] args)
        {
            var optionsBuilder = new DbContextOptionsBuilder<LibraryDbContext>();
            optionsBuilder.UseSqlServer("Data Source=DESKTOP-9FP7ODG;Database=MovieApp;Trusted_Connection=True;TrustServerCertificate=True");

            return new LibraryDbContext(optionsBuilder.Options);
        }
    }
}
