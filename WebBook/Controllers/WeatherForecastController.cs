﻿using Microsoft.AspNetCore.Mvc;

[ApiController]
[Route("books")]
public class BookController : ControllerBase
{
    private readonly IUnitOfWork _unitOfWork;

    // Sử dụng Dependency Injection để nhận IUnitOfWork làm tham số của constructor
    public BookController(IUnitOfWork unitOfWork)
    {
        _unitOfWork = unitOfWork;
    }

    // GET /books
    [HttpGet]
    public async Task<IActionResult> GetAllBooks()
    {
        var books = await _unitOfWork.BookRepository.GetAllBooksAsync();
        return Ok(books);
    }

    // POST /books
    [HttpPost]
    public async Task<IActionResult> AddBook([FromBody] Book book)
    {
        await _unitOfWork.BookRepository.AddBookAsync(book);
        return CreatedAtAction(nameof(GetBookById), new { id = book.Id }, book);
    }

    // GET /books/:id
    [HttpGet("{id}")]
    public async Task<IActionResult> GetBookById(int id)
    {
        var book = await _unitOfWork.BookRepository.GetBookByIdAsync(id);
        if (book == null)
        {
            return NotFound();
        }
        return Ok(book);
    }

    // PUT /books/:id
    [HttpPut("{id}")]
    public async Task<IActionResult> UpdateBook(int id, [FromBody] Book book)
    {
        if (id != book.Id)
        {
            return BadRequest();
        }
        await _unitOfWork.BookRepository.UpdateBookAsync(book);
        return NoContent();
    }

    // DELETE /books/:id
    [HttpDelete("{id}")]
    public async Task<IActionResult> DeleteBook(int id)
    {
        await _unitOfWork.BookRepository.DeleteBookAsync(id);
        return NoContent();
    }
}