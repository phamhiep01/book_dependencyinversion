﻿using WebBook;

public class UnitOfWork : IUnitOfWork
{
    private readonly LibraryDbContext _context;
    private IBookRepository _bookRepository;

    // Sử dụng Dependency Injection để nhận DbContext làm tham số của constructor
    public UnitOfWork(LibraryDbContext context)
    {
        _context = context;
    }

    public IBookRepository BookRepository
    {
        get
        {
            return _bookRepository ??= new BookRepository(_context);
        }
    }

    public async Task SaveAsync()
    {
        await _context.SaveChangesAsync();
    }

    public void Dispose()
    {
        _context.Dispose();
    }
}