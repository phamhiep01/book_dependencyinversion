﻿using WebBook;

public interface IUnitOfWork : IDisposable
{
    IBookRepository BookRepository { get; }
    Task SaveAsync();
}